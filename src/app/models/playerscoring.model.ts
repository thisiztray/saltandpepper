export class PlayerScoring {
  id: string;
  name: string;
  team: {
    isNBAFranchise: boolean;
    isAllStar: boolean;
    city: string;
    altCityName: string;
    fullName: string;
    tricode: string;
    teamId: string;
    nickname: string;
    urlName: string;
    teamShortName: string;
    confName: string;
    divName: string;
  };
  isOnCourt: boolean;
  clock: string;
  isGameActivated: boolean;
  score: number;
}
