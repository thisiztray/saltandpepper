export class Player {
  id: string;
  firstName: string;
  lastName: string;
  temporaryDisplayName: string;
  teamId: string;
  jersey: string;
  isActive: boolean;
  pos: string;
  heightMeters: string;
  weightKilograms: string;
  dateOfBirthUTC: string;
  draft: {
    teamId: string;
    pickNumber: string;
    roundNumber: string;
    year: string;
  };
  nbaDebutYear: string;
  yearsPro: string;
  college: string;
  country: string;
}
