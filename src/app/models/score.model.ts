export class Score {
  player: string;
  team: string;
  score: string;
  time: string;
}
