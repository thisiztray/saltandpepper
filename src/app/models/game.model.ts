export class Game {
  gameId: string;
  arena: {
    name: string;
    city: string;
    stateAbbr: string;
  };
  isGameActivated: boolean;
  startTimeUTC: string;
  gameUrlCode: string;
  clock: string;
  period: {
    current: number;
    isHalftime: boolean;
    isEndOfPeriod: boolean;
  };
  vTeam: {
    teamId: string;
    triCode: string;
    win: string;
    loss: string;
    score: string;
    linescore: [];
  };
  hTeam: {
    teamId: string;
    triCode: string;
    win: string;
    loss: string;
    score: string;
    linescore: [];
  };
}
