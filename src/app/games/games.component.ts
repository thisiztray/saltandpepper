import { Component, OnInit } from '@angular/core';
import {ApinbaService} from "../services/apinba.service";
import {MatTableDataSource} from "@angular/material/table";
import { faSync, faClock } from "@fortawesome/free-solid-svg-icons";
import {Game} from "../models/game.model";
import {DatePipe} from "@angular/common";
import {FormControl} from "@angular/forms";
import {interval, Subscription} from "rxjs";

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {

  public minDate: Date;

  constructor(private nba: ApinbaService,
              private datePipe: DatePipe) {
    this.minDate = new Date('2016-10-01');
  }

  public displayedColumns: string[] = ['hTeam', 'hScore', 'vScore', 'vTeam', 'period', 'arena'];
  public gamelist: MatTableDataSource<Game>;
  public loading = false;
  public fraDate: Date;
  public usaDate: Date;
  public dDay: FormControl;
  private updateSub: Subscription;

  public faSync = faSync;
  public faClock = faClock;

  ngOnInit(): void {
    this.fraDate = new Date(Date.now());
    this.usaDate = new Date(Date.now());
    this.usaDate.setHours(this.fraDate.getHours() - 17);
    this.dDay = new FormControl(this.usaDate);
    this.getGames();

    this.updateSub = interval(60000).subscribe(
      (val) => {
        this.getGames();
      }
    );
  }

  getGames() {
    this.loading = true;
    const dateFormat = this.datePipe.transform(this.usaDate, 'yyyyMMdd');
    return this.nba.getGames(dateFormat)
      .then(result => {
        this.gamelist = new MatTableDataSource(result as Game[]);
        this.loading = false;
      })
      .catch(error => console.log(error));
  }

  onChange(date) {
    this.usaDate = date;
    this.getGames();
  }

}
