import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ApinbaService} from "../services/apinba.service";
import {DatePipe} from "@angular/common";
import { faSync, faBasketballBall } from "@fortawesome/free-solid-svg-icons";
import {interval, Subscription} from "rxjs";
import {PlayerScoring} from "../models/playerscoring.model";
import {FormControl} from "@angular/forms";
import {MatSort, Sort} from "@angular/material/sort";

@Component({
  selector: 'app-livescore',
  templateUrl: './livescore.component.html',
  styleUrls: ['./livescore.component.scss']
})
export class LivescoreComponent implements OnInit {

  public minDate: Date;
  public maxDate: Date;

  constructor(private nba: ApinbaService,
              private datePipe: DatePipe) {
    this.minDate = new Date('2016-10-01');
    this.maxDate = new Date(Date.now());
  }

  public displayedColumns: string[] = ['player', 'team', 'score', 'clock'];
  public boxscore: PlayerScoring[];
  public maxScore: number;
  public loading = false;
  public fraDate: Date;
  public usaDate: Date;
  public dDay: FormControl;
  public orderBy: Sort;
  public gamesFilter = false;
  private updateSub: Subscription;

  public faSync = faSync;
  public faBasketballBall = faBasketballBall;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this.fraDate = new Date(Date.now());
    this.usaDate = new Date(Date.now());
    this.usaDate.setHours(this.fraDate.getHours() - 17);
    this.dDay = new FormControl(this.usaDate);
    this.getResults();

    this.updateSub = interval(60000).subscribe(
      (val) => {
        this.getResults();
      }
    );
  }

  getResults() {
    this.loading = true;
    const dateFormat = this.datePipe.transform(this.usaDate, 'yyyyMMdd');
    return this.nba.getBoxscore(dateFormat, this.gamesFilter)
      .then(result => {
        this.boxscore = result['playerlist'] as PlayerScoring[];
        this.maxScore = result['maxScore'];
        this.loading = false;
        if (this.orderBy) {
          this.sortData(this.orderBy)
        }
      })
      .catch(error => console.log(error));
  }

  onChange(date) {
    this.usaDate = date;
    this.getResults();
  }

  filter() {
    this.getResults();
  }

  sortData(sort: Sort) {
    this.orderBy = sort;
    const data = this.boxscore.slice();
    if (!sort.active || sort.direction === '') {
      this.boxscore = data;
      return;
    }

    this.boxscore = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'player': return compare(a.name, b.name, isAsc);
        case 'team': return compare(a.team.fullName, b.team.fullName, isAsc);
        case 'score': return compare(a.score, b.score, isAsc);
        case 'clock': return compare(a.clock, b.clock, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
