import { Component, OnInit } from '@angular/core';
import {ApinbaService} from "../services/apinba.service";
import { faSync } from "@fortawesome/free-solid-svg-icons";
import {Player} from "../models/player.model";
import {MatTableDataSource} from "@angular/material/table";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

  constructor(private nba: ApinbaService,
              private datePipe: DatePipe) { }

  public displayedColumns: string[] = ['displayName', 'team', 'position', 'height', 'weight', 'yearsPro', 'country'];
  public playerlist: MatTableDataSource<Player>;
  public loading = false;
  public fraDate: Date;

  public faSync = faSync;

  ngOnInit(): void {
    this.fraDate = new Date(Date.now());
    this.getPlayers();
  }

  getPlayers() {
    this.loading = true;
    const dateFormat = this.datePipe.transform(this.fraDate, 'yyyy');
    return this.nba.getPlayers(dateFormat)
      .then(result => {
        this.playerlist = new MatTableDataSource(result as Player[]);
        this.loading = false;
      })
      .catch(error => console.log(error));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.playerlist.filter = filterValue.trim().toLowerCase();
  }

}
