import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LivescoreComponent} from "./livescore/livescore.component";
import {PlayersComponent} from "./players/players.component";
import {GamesComponent} from "./games/games.component";

const routes: Routes = [
  {path: '', component: LivescoreComponent},
  {path: 'players', component: PlayersComponent},
  {path: 'games', component: GamesComponent},
  {path: '**', component: LivescoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
