import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {Player} from "../models/player.model";

@Injectable()
export class ApinbaService {

  public apiUrl: string;

  constructor(private httpClient: HttpClient) {
    this.apiUrl = environment.api;
  }

  public httpOptions = {
    headers: new HttpHeaders({

    })
  };

  getBoxscore(filterDate, gamesFilter) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.apiUrl + '/nba/boxscore/' + filterDate + '/' + gamesFilter, this.httpOptions)
        .subscribe(
          (response) => {
            resolve(response)
          },
          (error) => {
            reject(error);
          }
        )
    });
  }

  getGames(filterDate) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.apiUrl + '/nba/games/' + filterDate, this.httpOptions)
        .subscribe(
          (response) => {
            resolve(response)
          },
          (error) => {
            reject(error);
          }
        )
    });
  }

  getPlayers(filterYear) {
    return new Promise((resolve, reject) => {
      this.httpClient.get<Player[]>(this.apiUrl + '/nba/players/' + filterYear, this.httpOptions)
        .subscribe(
          (response) => {
            resolve(response)
          },
          (error) => {
            reject(error);
          }
        )
    });
  }

}
